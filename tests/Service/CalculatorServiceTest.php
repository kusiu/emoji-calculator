<?php

namespace App\Tests\Service;

use App\Form\CalculatorType;
use App\Service\CalculatorService;
use PHPUnit\Framework\TestCase;

class CalculatorTest extends TestCase
{
    public function testAlien()
    {
        $calculator = new CalculatorService(1, 1, CalculatorType::OPERATOR_ADD);
        $result = $calculator->calculate();
        $this->assertEquals(2, $result);
    }

    public function testSkull()
    {
        $calculator = new CalculatorService(10, 5, CalculatorType::OPERATOR_SUBTRACT);
        $result = $calculator->calculate();
        $this->assertEquals(5, $result);
    }

    public function testGhost()
    {
        $calculator = new CalculatorService(2, 2, CalculatorType::OPERATOR_MULTIPLY);
        $result = $calculator->calculate();
        $this->assertEquals(4, $result);
    }

    public function testScream()
    {
        $calculator = new CalculatorService(2, 2, CalculatorType::OPERATOR_DIVIDE);
        $result = $calculator->calculate();
        $this->assertEquals(1, $result);
    }
}