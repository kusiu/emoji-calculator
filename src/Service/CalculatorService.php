<?php

namespace App\Service;

use App\Form\CalculatorType;

class CalculatorService
{
    private $operand1;

    private $operand2;

    private $operator;

    public function __construct(int $operand1, int $operand2, string $operator)
    {
        $this->operand1 = $operand1;
        $this->operand2 = $operand2;
        $this->operator = $operator;
    }

    public function calculate()
    {
        switch ($this->operator) {
            case CalculatorType::OPERATOR_ADD:
                return $this->operand1 + $this->operand2;
                break;

            case CalculatorType::OPERATOR_SUBTRACT:
                return $this->operand1 - $this->operand2;
                break;

            case CalculatorType::OPERATOR_MULTIPLY:
                return $this->operand1 * $this->operand2;
                break;

            case CalculatorType::OPERATOR_DIVIDE:
                if (0 === $this->operand2) {
                    throw new \Exception('Divide by 0 is not allowed.');
                }
                
                return $this->operand1 / $this->operand2;
                break;

            default:
                throw new \Exception('Unknown operator');
                break;
        }
    }
}