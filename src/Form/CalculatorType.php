<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;

class CalculatorType extends AbstractType
{
    const OPERATOR_ADD = '+';
    const OPERATOR_SUBTRACT = '-';
    const OPERATOR_MULTIPLY = '*';
    const OPERATOR_DIVIDE = '/';

    const OPERATOR_LIST = [
        '👽' => self::OPERATOR_ADD,
        '💀' => self::OPERATOR_SUBTRACT,
        '👻' => self::OPERATOR_MULTIPLY,
        '😱' => self::OPERATOR_DIVIDE
    ];

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('operand1', IntegerType::class, [
                'label' => false,
                'required' => true,
                'attr' => [
                    'id' => 'operand1'
                ]
            ])
            ->add('operator', ChoiceType::class, [
                'choices' => self::OPERATOR_LIST,
                'label' => false,
                'required' => true,
                'attr' => [
                    'id' => 'operator'
                ]
            ])
            ->add('operand2', IntegerType::class, [
                'label' => false,
                'required' => true,
                'attr' => [
                    'id' => 'operand2'
                ]
            ])
        ;
    }
}