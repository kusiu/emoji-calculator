<?php

namespace App\Controller;

use App\Service\CalculatorService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AjaxController extends AbstractController
{
    /**
     * @Route(
     *      "/calculate",
     *      name="calculate",
     *      methods={"POST"},
     *      defaults={"_format": "json"}
     * )
     */
    public function calculate(Request $request)
    {
        try {
            $data = $request->request->get('calculator');
            $calculatorService = new CalculatorService($data['operand1'], $data['operand2'], $data['operator']);
            $result = $calculatorService->calculate();
            return new JsonResponse($result, Response::HTTP_OK);
        } catch (\Exception $exception) {
            return new JsonResponse($exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}